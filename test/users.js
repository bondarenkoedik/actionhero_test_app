process.env.NODE_ENV = 'test';

var should = require('should');
var actionheroPrototype = require('actionhero').actionheroPrototype;
var actionhero = new actionheroPrototype();
var api;

describe('actionhero app tests', function() {

  before(function(done) {
    actionhero.start(function(err, a) {
      api = a;
      done();
    });
  });

  beforeEach(function(done) {

    api.database.User.create({username: "JohnDoe", password: "12345", email: "johndoe@gmail.com"}, (err, obj) => {
      if (err) {
        console.log(err);
      }
      done();
    });
 
  });

  after(function(done) {
    actionhero.stop(function(err) {
      if (err) {
        console.log(err);
      }
      done();
    });
  });

  afterEach(function(done) {

    api.database.User.remove({}, (err) => {
      if (err) {
        console.log(err);
      }
      done();
    });

  });

  it('lists the users', function(done) {
    api.specHelper.runAction('usersList', (response) => {
      response.users.should.be.an.Array;
      done();
    });
  });

  it('get user with invalid id', function(done) {
    api.specHelper.runAction('usersShowOne', {id: "someinvalidid"}, (response) => {
      should.exist(response.error);
      done();
    });
  });

  it('create new user', function(done) {

    // this.timeout(500);

    const testUser = {
      username: "JohnDoe",
      email: "john@gmail.com",
      password: "12345"
    };
    
    api.specHelper.runAction('usersCreateNewOne', testUser, (response) => {
      should.not.exist(response.error);
      done();
    });
  });

});
