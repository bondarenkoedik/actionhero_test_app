'use strict';

// COMPONENTS

var createUserComponents = {
  username: {
    required: true,
    validator(param) {
      var re = /^[a-zA-Z0-9]+$/;
      if (param.length < 3) {
        return new Error('username should be at least 3 letters long');
      } else if( re.test(param) ) {
        return true;
      } else {
        return new Error('that is not a valid username. Only alphabet symbols and digits.');
      }
    },  
  },
  email: {
    required: true,
    validator(param) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if( re.test(param) ) {
        return true;
      } else {
        return new Error('that is not a valid email address');
      }
    },  
  },
  password: {
    required: true,
    validator(param) {
      if (param.length < 4) {
        return new Error('password should be at least 4 letters long');
      } else {
        return true;
      }
    },
    formatter(param) {
      return String(param);
    },
  }
};


// ACTIONS

exports.usersList = {
  name: "usersList",
  description: "I show the list of all users",
  authenticated: false,
  version: 1.0,
  run(api, data, next) {
    // data.response.users = [];
    // next();
    api.users.list((err, users) => {
      data.response.users = users;
      next(err);
    });
  }
};


exports.usersShowOne = {
  name: "usersShowOne",
  description: "I show info about one particular user",
  authenticated: false,
  version: 1.0,
  inputs: {
    id: { required: true }
  },
  run(api, data, next) {
    api.users.get(data.params.id, (err, user) => {
      if (!user) {
        data.connection.rawConnection.responseHttpCode = 404;
      }
      data.response.user = user;
      next(err);
    });
  }
};

exports.usersCreateNewOne = {
  name: "usersCreateNewOne",
  description: "I create new user and return JSON of this user",
  authenticated: false,
  version: 1.0,
  inputs: createUserComponents,
  run(api, data, next) {
    api.users.add(data.params.username, data.params.email, data.params.password, (err, user) => {
      data.response.user = user;
      next(err);
    });
  }
};

