'use strict';


module.exports = {
  
  loadPriority:  1000,
  
  initialize(api, next) {

    // write test data

    // api.database.User.create({username: "Eduard", password: "12345", email: "test@gmail.com"}, (err, obj) => {
    //   if (err) {
    //     console.log(err);
    //     next(err);
    //   } else {
    //     console.log("Object is created");
    //   }
    // });
   
    api.users = {
      
      add(username, email, password, next) {
        api.database.User.findOne({ email }, (err, user) => {
          if (err) {
            next(err);
          } else if (user) {
            next("User with such email already exists.")
          } else {
            api.database.User.create({ username, email, password }, (err, user) => {
              if (err) {
                next(err);
              } else {
                next(err, user);
              }
            });
          }
        });
      },

      get(id, next) {
        api.database.User.findById(id, (err, user) => {
          next(err, user);
        });
      },

      list(next) {
        api.database.User.find({}, (err, users) => {
          next(err, users);
        });
      },

    };

    next();
  } // initialize method
};
