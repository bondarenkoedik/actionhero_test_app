'use strict';

const mongoose = require("mongoose");

const User = require("../models/user");

module.exports = {
  
  loadPriority:  500, // I am now sure is it okay to use loadPriority in order to initialize database before users middleware
  
  initialize(api, next) {

    mongoose.connect(api.config.general.database.path);

    api.database = { User };

    next();
  },

};
